/* SPDX-License Identifier: AGPL-3.0-or-later
 *
 * Copyright (C) 2021 EVBox Intelligence B.V.
 * Copyright (C) 2021 Olliver Schinagl <oliver@schinagl.nl>
 *
 * Configuration is processed in 'config.js'. Avoid editing this file.
 *
 */

/* Allow opt-out by default when scanning for projects */
const dashboard_default_opt_out = ((typeof DASHBOARD_DEFAULT_OPT_OUT !== "undefined") &&
                                   DASHBOARD_DEFAULT_OPT_OUT) ? DASHBOARD_DEFAULT_OPT_OUT : false;

/*
 * Default HTTP headers for the API. Force avoiding of caching, as we need
 * up-to-date results from the API.
 */
const gitlab_http_headers = {
	"Pragma": "no-cache",
	"Cache-Control": "no-cache",
	"Content-Type": "application/json",
	"Private-Token": ((typeof DASHBOARD_ACCESS_TOKEN !== "undefined") &&
                          DASHBOARD_ACCESS_TOKEN &&
                          (DASHBOARD_ACCESS_TOKEN.length > 0)) ?
                         DASHBOARD_ACCESS_TOKEN :
                         "",
};

/* Date format for wide tiles */
const datetime_display_wide = {
	weekday: "long",
	month: "long",
	day: "numeric",
	year: "numeric",
};

/* Date format for narrow tiles */
const datetime_display_narrow = {
	weekday: "short",
	month: "short",
	day: "numeric",
};

/* Helper constants to improve readability on _basic_ time/date calculations */
const SECOND = 1;
const MINUTES = (60 * SECOND);
const HOURS = (60 * MINUTES);
const DAYS = (24 * HOURS);
const WEEKS = (7 * DAYS);
const MONTHS = (30 * DAYS);
const YEARS = (365 * DAYS);

/*
 * Global variable containing the preferred locale. The configuration file
 * parameter takes precedence, followed by the preferred entry from the browser.
 */
const user_locale = ((typeof DASHBOARD_USER_LOCALE !== "undefined") &&
                     DASHBOARD_USER_LOCALE &&
                     (DASHBOARD_USER_LOCALE.length > 0)) ?
                    DASHBOARD_USER_LOCALE :
                    (navigator.language) ? navigator.language : "en-NL";

/* Static tiles, rendered first, order is kept as is. */
let dashboard_static_projects = ((typeof DASHBOARD_PROJECTS_STATIC !== "undefined") &&
                                 DASHBOARD_PROJECTS_STATIC) ? DASHBOARD_PROJECTS_STATIC : [];
/* Dynamic tiles, rendered after static, ordering based on update state. */
let dashboard_dynamic_projects = ((typeof DASHBOARD_PROJECTS_DYNAMIC !== "undefined") &&
                                  DASHBOARD_PROJECTS_DYNAMIC) ? DASHBOARD_PROJECTS_DYNAMIC : [];

/**
 * get_time_string() - Convert a UNIX timestamp into time-string
 * timestamp:          UNIX timestamp, in seconds, to convert
 *
 * Converts the passed UNIX timestamp into a time-string. Seconds wrap to
 * minutes and minutes wrap to hours. E.g. 2 days, 15 minutes and 2 seconds
 * will show as "48:15:02". Single digits are prefixed with 0.
 *
 * Returns:            Time as string "hours:minutes:seconds" e.g. "00:11:22"
 *                     on success, an error string on failure.
 */
function get_time_string(timestamp)
{
	let hours = 0;
	let minutes = 0;
	let seconds = 0;

	if (!(Number.isInteger(timestamp)))
		return "Not a number";

	hours = Math.round(Math.floor(timestamp / HOURS));
	timestamp -= (hours * HOURS);

	minutes = Math.round(Math.floor(timestamp / MINUTES));
	timestamp -= (minutes * MINUTES);

	seconds = Math.round(Math.floor(timestamp));

	if (hours < 10)
		hours = "0" + hours;

	if (minutes < 10)
		minutes = "0" + minutes;

	if (seconds < 10)
		seconds = "0" + seconds;

	return hours + ":" + minutes + ":" + seconds;
}

/**
 * get_readable_time() - Convert a UNIX timestamp into human readable time
 * timestamp:            UNIX timestamp, in seconds, to convert
 *
 * Converts the passed UNIX timestamp into a human readable time. Seconds wrap
 * to minutes and minutes wrap to hours. E.g. 2 days, 15 minutes and 2 seconds
 * will show as "2 days 1 minute 2 seconds". Values greater than 1 get an 's'
 * appended. E.g. 1 minute vs 2 minutes.
 *
 * Returns:              Time as human readable string, for example
 *                       "2 days 5 minutes 1 second" on success, an error string
 *                       on failure.
 */
function get_readable_time(timestamp)
{
	let hours, minutes, seconds;
	let readable_time = "";

	if (typeof timestamp !== "number")
		return "Not a number";

	hours = Math.round(Math.floor(timestamp / HOURS));
	timestamp -= (hours * HOURS);

	minutes = Math.round(Math.floor(timestamp / MINUTES));
	timestamp -= (minutes * MINUTES);

	seconds = Math.round(Math.floor(timestamp));

	if (readable_time.length > 0)
		readable_time += " ";

	if (hours == 1)
		readable_time += hours + " hour";
	else if (hours > 1)
		readable_time += " hours";

	if (readable_time.length > 0)
		readable_time += " ";

	if (minutes == 1)
		readable_time += minutes + " minute"
	else if (minutes > 1)
		readable_time += " minutes";

	if (readable_time.length > 0)
		readable_time += " ";

	if (seconds == 1)
		readable_time += seconds + " second";
	else if (seconds > 1)
		readable_time += " seconds";

	return readable_time.trim();
}

/**
 * get_short_readable_time() - Convert a UNIX timestamp into short human readable time
 * timestamp:                  UNIX timestamp, in seconds, to convert
 *
 * Converts the passed UNIX timestamp into a short human readable time. Seconds
 * are rounded to minutes, minutes rounded to hours, hours rounded to days etc.
 * E.g. 2 days, 15 minutes and 2 seconds will show as "2 days". Values greater
 * than one get an 's' appended. E.g. 1 minute vs 2 minutes.
 *
 * Returns:              Time as human readable string e.g. "5 minutes" on
 *                       success, an error string on failure.
 */
function get_short_readable_time(timestamp)
{
	let timestamp_unit = "";

	if (typeof timestamp !== "number")
		return "Not a number";

	if (timestamp > YEARS) {
		timestamp = Math.round(timestamp / YEARS);
		if (timestamp == 1)
			timestamp_unit = " year";
		else
			timestamp_unit = " years"
	} else if (timestamp > MONTHS) {
		timestamp = Math.round(timestamp / MONTHS);
		if (timestamp == 1)
			timestamp_unit = " month";
		else
			timestamp_unit = " months"
	} else if (timestamp > WEEKS) {
		timestamp = Math.round(timestamp / WEEKS);
		if (timestamp == 1)
			timestamp_unit = " week";
		else
			timestamp_unit = " weeks"
	} else if (timestamp > DAYS) {
		timestamp = Math.round(timestamp / DAYS);
		if (timestamp == 1)
			timestamp_unit = " day";
		else
			timestamp_unit = " days"
	} else if (timestamp > HOURS) {
		timestamp = Math.round(timestamp / HOURS);
		if (timestamp == 1)
			timestamp_unit = " hour";
		else
			timestamp_unit = " hours"
	} else if (timestamp > MINUTES) {
		timestamp = Math.round(timestamp / MINUTES);
		if (timestamp == 1)
			timestamp_unit = " minute";
		else
			timestamp_unit = " minutes"
	} else {
		timestamp = Math.round(timestamp);
		if (timestamp == 1)
			timestamp_unit = " second";
		else
			timestamp_unit = " seconds"
	}

	return timestamp + timestamp_unit;
}

/**
 * get_coverage_name() - Convert a coverage number into a matching name
 * coverage:             Coverage percentage as number [0, 100]
 *
 * Prints a string matching the supplied percentage of coverage. E.g. Coverage
 * of 100% results in "Outstanding" and a coverage between 40% and 60% is deemed
 * "Mediocre". For all combinations see the code.
 *
 * Returns:               A string converting the number into a string on
 *                        success, "Appalling" failure.
 */
function get_coverage_name(coverage)
{
	if (coverage >= 100)
		return "Outstanding";

	if (coverage > 95)
		return "Excellent";

	if (coverage > 85)
		return "Very Good";

	if (coverage > 75)
		return "Good";

	if (coverage > 60)
		return "Mediocre";

	if (coverage > 40)
		return "Poor";

	if (coverage > 20)
		return "Very Poor";

	return "Appalling";
}

/**
 * get_coverage_class() - Convert a coverage number into a matching className
 * coverage:              Coverage percentage as number [0, 100]
 *
 * Prints a string matching the supplied percentage of coverage. E.g. Coverage
 * of 100% results in "outstanding" and a coverage between 40% and 60% is deemed
 * "mediocre". For all combinations see the code.
 *
 * Returns:               A string converting the number into a string on
 *                        success, "appalling" failure.
 */
function get_coverage_class(coverage)
{
	if (coverage >= 100)
		return "outstanding";

	if (coverage > 95)
		return "excellent";

	if (coverage > 85)
		return "very_good";

	if (coverage > 75)
		return "good";

	if (coverage > 60)
		return "mediocre";

	if (coverage > 40)
		return "poor";

	if (coverage > 20)
		return "very_poor";

	return "appalling";
}

/* Global static error counter to keep track of the number of occurred errors */
let error_count = 0;
/**
 * handle_fetch_error() - Print fetch errors in the 'heartbeat's date field
 * error:                 String containing the occurred error.
 *
 * If a fetch error occurs, this function ensures it is printed in the 'date'
 * field of the heartbeat tile. Further more, errors are accumulated and if
 * the threshold set by @DASHBOARD_ERROR_RELOAD_COUNT is exceeded, the page
 * as a whole is reloaded.
 *
 * Note, that technically, any programmer error that throws an error as part
 * of the fetch function handler will trigger an error this way.
 */
function handle_fetch_error(error)
{
	document.getElementById("time").textContent = "System failure!";
	document.getElementById("date").textContent = error + " [" + error_count + "]";

	if ((typeof DASHBOARD_ERROR_RELOAD_COUNT === "number") &&
	    (error_count++ >= DASHBOARD_ERROR_RELOAD_COUNT))
		window.location.reload();
}

/**
 * check_fetch_response() - Check the response code of a fetch call
 * tile_id:                 Tile ID that caused the fetch error
 * origin:                  The source of the error
 * response:                The fetch response
 *
 * Checks the @response of fetch, and if not 'ok' prints an error on the div
 * using @tile_id that caused the error. The @origin of the error is just a
 * string for display purposes and can help to trace the origin of the error.
 */
function check_fetch_response(tile_id, origin, response)
{
	if (!response.ok) {
		let tile = document.querySelector(String(tile_id).includes("-uid-") ?
		           `[id*="${tile_id.match(/\d+/)[0]}"]` :
		           `[id="${tile_id}"]`);

		if (tile)
			tile.textContent = "Unable to retrieve data!";

		throw Error(origin + ": " + response.statusText);
	}

	return response;
}

/**
 * create_pipeline_measurand() - Find or create a div element for @measurand_type
 * tile_id:                      The tile ID to create data for
 * measurand_type:               The type of measurand (string)
 *
 * Finds an existing 'div' element or creates a new 'div' element with the ID
 * set to "@tile_id-pipeline-@measurand_type" and the css className set to
 * "pipeline-@measurand_type". The newly created 'div' element is added as a
 * child to the 'div' element named '@tile_id'.
 *
 * Returns:                      The found or created 'div' element on success
 *                               null otherwise.
 */
function create_pipeline_measurand(tile_id, measurand_type)
{
	let tile_div = null;

	if (!(tile_div = document.getElementById(tile_id + "-pipeline-" + measurand_type))) {
		tile_div = document.createElement("div");
		tile_div.id = tile_id + "-pipeline-" + measurand_type;
		tile_div.className = "pipeline-" + measurand_type;
		document.getElementById(tile_id).appendChild(tile_div);
	}

	return tile_div;
}

/**
 * update_coverage_data() - Create or update the coverage measurand tile entry
 * tile_id:                 The tile ID to fill the coverage data with
 * path_with_namespace:     GitLab path with namespace used for the coverage URL
 * coverage:                Number indicating coverage results.
 *
 * Updates the 'coverage' div as created by @create_pipeline_measurand with the
 * coverage data, which includes the URL towards the coverage report that is
 * part of the artifacts/pages of the related repository. The file expected in
 * that location is 'coverage/coverage.html'.
 *
 * The coverage color is set via the css className 'coverage_NAME' where NAME
 * is generated from @get_coverage_name.
 */
function update_coverage_data(tile_id, path_with_namespace, coverage)
{
	const root_namespace_separator_index = path_with_namespace.indexOf("/");
	const root_namespace = path_with_namespace.slice(0, root_namespace_separator_index);
	const repo_path = path_with_namespace.slice(root_namespace_separator_index);
	let coverage_text = "Coverage: " + coverage + "%";
	let tile_div = null;

	tile_div = create_pipeline_measurand(tile_id, "coverage");
	tile_div.title = get_coverage_name(coverage);
	tile_div.className = "pipeline-coverage";

	if ((typeof CI_SERVER_PROTOCOL !== "undefined") &&
	    CI_PAGES_DOMAIN &&
	    (CI_PAGES_DOMAIN.length > 0) &&
	    (typeof CI_PAGES_DOMAIN !== "undefined") &&
	    CI_PAGES_DOMAIN &&
	    (CI_PAGES_DOMAIN.length > 0)) {
		let link = tile_div.getElementsByTagName("a")[0];

		if (!link)
			link = document.createElement("a");

		link.href = CI_SERVER_PROTOCOL + "://" + root_namespace + "." +
		            CI_PAGES_DOMAIN + repo_path + "/coverage/coverage.html";
		link.textContent = coverage_text;
		tile_div.appendChild(link);
	} else {
		tile_div.textContent = coverage_text;
	}

	if ((typeof DASHBOARD_COVERAGE !== "undefined") &&
	    (DASHBOARD_COVERAGE)) {
		if (coverage <= 0)
			tile_div.textContent = "No Code Coverage!";

		document.getElementById(tile_id).className += " coverage_" + get_coverage_class(coverage);
	}
}

/**
 * update_queued_time() - Create or update the queued time measurand
 * tile_id:               The tile ID to fill the queued time data with
 * pipeline_data:         GitLab pipeline data object
 *
 * Updates the 'queued' div as created by @create_pipeline_measurand with the
 * queued time, if available. The queued time is the start time minus the
 * created time as set in @pipeline_data.
 */
function update_queued_time(tile_id, pipeline_data)
{
	const created_at = new Date(pipeline_data.created_at).valueOf();
	let started_at = new Date(pipeline_data.started_at).valueOf();
	let tile_div = null;

	tile_div = create_pipeline_measurand(tile_id, "queued");
	if (!started_at || (started_at <= 0))
		started_at = Date.now();

	tile_div.textContent = "Queued: " +
	                       get_short_readable_time(Math.round((started_at - created_at) / 1000));
	tile_div.title = pipeline_data.created_at;
}

/**
 * update_status_times - Create or update the status time measurand
 * tile_id:              The tile ID to fill the status time data with
 * pipeline_data:        GitLab pipeline data object
 * pipeline_now:         The current time to reference against
 *
 * Updates the 'finished_at' and 'runtime' div as created by
 * @create_pipeline_measurand with the run time elapsed so far, or the finished
 * run time with the times as set in @pipeline_data. The @pipeline_now timestamp
 * can be passed as 'new Date()' but it is recommended to pass the time from
 * when the start of the acquisition cycle to more accurately match the data in
 * @pipeline_data.
 */
function update_status_times(tile_id, pipeline_data, pipeline_now)
{
	const finished_at = new Date(pipeline_data.finished_at).valueOf();
	const started_at = new Date(pipeline_data.started_at).valueOf();
	const now = pipeline_now.valueOf();
	let finishtime = null;
	let tile_div = null;
	let runtime = null;

	switch (pipeline_data.detailed_status.group) {
	case "canceled": /* fall-through */
	case "failed": /* fall-through */
	case "success": /* fall-through */
	case "success-with-warnings":
		runtime = get_short_readable_time(Math.round(now - finished_at) / 1000) + " ago";
		finishtime = "after " + get_short_readable_time(pipeline_data.duration);
		break;
	case "running":
		runtime = get_short_readable_time(Math.round((now - started_at) / 1000));
		finishtime = "for";
		break;
	default:
		runtime = "No runtime"
		finishtime = "Not finished";
		break;
	}
	tile_div = create_pipeline_measurand(tile_id, "finished_at");
	tile_div.textContent = finishtime;
	tile_div.title = pipeline_data.finished_at;

	tile_div = create_pipeline_measurand(tile_id, "runtime");
	tile_div.textContent = runtime;
	tile_div.title = pipeline_data.started_at;
}

/**
 * update_status_data() - Create or update the status name measurand
 * tile_id:               The tile ID to fill the status name with
 * pipeline_data:         GitLab pipeline data object
 *
 * Updates the 'status' div as created by @create_pipeline_measurand with the
 * status name as set in @pipeline_data. The status itself is a link that points
 * to the pipeline itself, as set by the @pipeline_data's 'web_url'.
 */
function update_status_data(tile_id, pipeline_data)
{
	let tile_div = null;
	let link = null;

	link = document.createElement("a");
	link.textContent = pipeline_data.detailed_status.text;
	link.title = pipeline_data.detailed_status.label;
	link.href = pipeline_data.web_url;
	tile_div = create_pipeline_measurand(tile_id, "status");
	tile_div.textContent = "";
	tile_div.appendChild(link);
}

/**
 * update_branch_data() - Create or update the branch of a tile
 * tile_id:               The tile ID to fill the title with
 * repo_branch:           The branch name of the pipeline, if not the 'default_branch'
 * project_data:          GitLab project data object
 *
 * Updates the 'branch' div as created by @create_pipeline_measurand with the
 * repository name as branch, as set in @repo_branch. The branch itself is a link
 * that points to the project itself, as set by @project_data's 'web_url' and
 * the project name in the title.
 */
function update_branch_data(tile_id, repo_branch, project_data)
{
	let tile_div = null;
	let link = null;

	link = document.createElement("a");
	link.href = project_data.web_url;
	if (repo_branch && (repo_branch.length > 0))
		link.href += "/-/tree/" + repo_branch;
	link.textContent = repo_branch;
	tile_div = create_pipeline_measurand(tile_id, "branch");
	tile_div.title = project_data.name;
	tile_div.textContent = "";
	tile_div.appendChild(link);
}

/**
 * update_title_data() - Create or update the title of a tile
 * tile_id:              The tile ID to fill the title with
 * repo_branch:          The branch name of the pipeline, if not the 'default_branch'
 * project_data:         GitLab project data object
 *
 * Updates the 'title' div as created by @create_pipeline_measurand with the
 * repository name as title, as set in @project_data. The title itself is a link
 * that points to the project itself, as set by @project_data's 'web_url' and
 * the project ID as the title.
 */
function update_title_data(tile_id, repo_branch, project_data)
{
	let tile_div = null;
	let link = null;

	link = document.createElement("a");
	link.href = project_data.web_url;
	if (repo_branch && (repo_branch.length > 0))
		link.href += "/-/tree/" + repo_branch;
	link.textContent = project_data.name;
	tile_div = create_pipeline_measurand(tile_id, "title");
	tile_div.title = tile_id;
	tile_div.textContent = "";
	tile_div.appendChild(link);
}

/**
 * sort_dynamic_tiles() - Sort the dynamic tiles in the DOM
 * pipeline_tile:         The DIV element tile containing all the pipeline data
 * updated_at:            Timestamp in milliseconds as integer
 *
 * After having successfully added a tile, this function can be used to sort
 * them.
 *
 * It does so by looking at the last static tile, if static tiles are available
 * or uses the first tile inside the 'pipeline-grid' div element.
 *
 * Sorting then looks at the tile's ID property, which contains a timestamp
 * similar to @updated_at and compares this to the new @updated_at and moves
 * the tile before an older one.
 */
function sort_dynamic_tiles(pipeline_tile, updated_at)
{
	const dashboard_projects_static_last = ((typeof dashboard_static_projects !== "undefined") &&
	                                        dashboard_static_projects &&
	                                        (dashboard_static_projects.length > 0)) ?
	                                       dashboard_static_projects[dashboard_static_projects.length - 1] :
	                                       null;
	const tile_uid = dashboard_projects_static_last.id + "-uid-" + dashboard_projects_static_last.branch;
	const last_tile = document.getElementById(tile_uid);
	let tile = null;

	if ((dashboard_projects_static_last) &&
	    last_tile)
		tile = last_tile.nextSibling;
	else
		tile = document.getElementById("pipeline-grid").firstChild;

	for (; tile; tile = tile.nextSibling) {
		const tile_updated_at = parseInt(tile.title);

		if (tile.id == pipeline_tile.id)
			break;

		if (tile_updated_at > updated_at)
			continue;

		if (tile_updated_at <= updated_at) {
			tile.before(pipeline_tile);
			break;
		}
	}
}

/**
 * update_tile() - Create or update a pipeline tile within the "pipeline-grid"
 * tile_id:        The tile ID to create or update
 * pipeline_data:  GitLab pipeline data object
 *
 * Create or update a pipeline tile and add it to the div element with the ID
 * "pipeline-grid". The tile is appended to the existing list of tiles and
 * sorted based on its 'title' field, as supplied by the @pipeline_data.
 *
 * Additionally tiles that are part of the 'dynamic' list of ID's are sorted
 * to ensure the most recent updated tiles are at the top, but after the last
 * static tile. This basic algorithm does assume that this function is initially
 * called with the tile_id's in the correct order.
 */
function update_tile(tile_id, pipeline_data)
{
	const updated_at = new Date(pipeline_data.updated_at).valueOf();
	let pipeline_tile = null;

	if (!(pipeline_tile = document.getElementById(tile_id))) {
		const pipeline_tile_parent = document.getElementById("pipeline-grid");

		pipeline_tile = document.createElement("div");
		pipeline_tile.id = tile_id;
		pipeline_tile_parent.appendChild(pipeline_tile);
	} else {
		const pipeline_branch = document.getElementById(tile_id + "-pipline-branch");

		if ((pipeline_branch) && (pipeline_branch.querySelector("a").textContent))
			return;
	}
	pipeline_tile.title = updated_at;
	pipeline_tile.className = "grid-tile pipeline-tile status_" + pipeline_data.detailed_status.group;

	return pipeline_tile;
}

/**
 * update_heartbeat_tile() - Update the date and time of the heartbeat tile
 * timestamp:                UNIX timestamp to use for the heartbeat
 *
 * Updates the date and time of the div elements ID's as "time" and "date" if
 * a valid timestamp is passed.
 */
function update_heartbeat_tile(timestamp)
{
	if (!timestamp)
		return;

	document.getElementById("time").textContent = timestamp.toLocaleTimeString(user_locale);
	document.getElementById("date").textContent = timestamp.toLocaleDateString(user_locale, datetime_display_wide);
}

/**
 * render_pipeline_tile() - Render a pipeline tile on the grid
 * project_data:            GitLab project data object
 * pipeline_data:           GitLab pipeline data object
 * timestamp:               UNIX timestamp to use for the status time calculation
 *
 * Creates and updates a pipeline tile with all data fields. If there is no
 * project or pipeline data, this is reflected on the tile in that case.
 */
function render_pipeline_tile(project_data, pipeline_data, timestamp)
{
	const tile_uid = project_data.id + "-uid-" + pipeline_data.ref;
	let pipeline_tile = null;

	if (!project_data)
		throw Error("No project data");
	if (!pipeline_data)
		throw Error("No pipeline data");

	pipeline_tile = update_tile(tile_uid, pipeline_data);

	if ((typeof DASHBOARD_PROJECTS_STATIC !== "undefined") &&
	    !DASHBOARD_PROJECTS_STATIC.some(item => parseInt(item.id) === project_data.id))
		sort_dynamic_tiles(pipeline_tile, pipeline_tile.title);

	if ((typeof DASHBOARD_SHOW_TITLE === "undefined") || (DASHBOARD_SHOW_TITLE))
		update_title_data(tile_uid, pipeline_data.ref, project_data);
	if ((typeof DASHBOARD_SHOW_BRANCH === "undefined") || (DASHBOARD_SHOW_BRANCH))
		update_branch_data(tile_uid, pipeline_data.ref, project_data);
	if ((typeof DASHBOARD_SHOW_STATUS === "undefined") || (DASHBOARD_SHOW_STATUS))
		update_status_data(tile_uid, pipeline_data);
	if ((typeof DASHBOARD_SHOW_TIMES === "undefined") || (DASHBOARD_SHOW_TIMES)) {
		update_status_times(tile_uid, pipeline_data, timestamp);
		update_queued_time(tile_uid, pipeline_data);
	}
	if ((typeof DASHBOARD_SHOW_COVERAGE === "undefined") || (DASHBOARD_SHOW_COVERAGE))
		update_coverage_data(tile_uid, project_data.path_with_namespace, Number(pipeline_data.coverage));
}

/**
 * render_dashboard_content() - Render the dashboard grid
 * project_data:                GitLab project data object
 * pipeline_data:               GitLab pipeline data object
 * timestamp:                   UNIX timestamp to reference all 'now' moments against
 *
 * Create and update all tiles in the dashboard grid.
 *
 * Note, that @timestamp is best created as 'now' ('new Date()') at start of
 * data retrieval, to server as a consistent reference point but also to
 * indicate the last time of contact with the backend. As a 'heartbeat'.
 */
function render_dashboard_content(project_data, pipeline_data, timestamp)
{
	if (!timestamp)
		return;

	update_heartbeat_tile(timestamp);

	render_pipeline_tile(project_data, pipeline_data, timestamp)
}

/**
 * remove_duplicates() - Remove duplicate entries from an projects array
 * data                  Projects array containing at id and branch members
 *
 * Remove duplicate entries based on matching .id's and empty branches, and
 * also duplicate entries where both matches.
 *
 * Returns the number of duplicates that where removed
 */
function remove_duplicates(data)
{
	const dupe_count = 1;
	let duplicates = 0;

	if (!Array.isArray(data))
		return;

	for (let i = data.length - 1; i >= 0; i--) {
		if (data[i] &&
		    data[i].id &&
		    Number.isInteger(data[i].id) &&
		    (((!data[i].branch || (!data[i].branch.length)) &&
		     data.some(dupe => (dupe !== data[i]) &&
		                       Number.isInteger(dupe.id) &&
		                       (dupe.id === data[i].id) &&
		                       dupe.branch && dupe.branch.length)) ||
		    (data[i].branch &&
		     data[i].branch.length &&
		     data.some(dupe => (dupe !== data[i]) &&
		                       Number.isInteger(dupe.id) &&
		                       (dupe.id === data[i].id) &&
		                       dupe.branch &&
		                       dupe.branch.length &&
		                       (dupe.branch === data[i].branch))))) {
				console.log(`Warning: Removing duplicate ID: ${data[i].id}; ${data[i].branch}`);

				data.splice(i, dupe_count);
				duplicates += dupe_count;
			}
	}

	return duplicates;
}

/**
 * component_add() - Add a group or project component
 * component_id      GitLab ID
 * component_type    Type of component being added ("group", "project")
 *
 * Depending on the component type, add the group or project to the list of
 * dynamic project ID's. The following GitLab are queried and honored:
 *
 * - DASHBOARD_OPT_OUT: If this project is part of a sub-group, do not add
 * - DASHBOARD_OPT_IN:  If global/default opt-out is enabled, only add if set
 * - DASHBOARD_TILE_BRANCHES: An array of specific branches to add
 */
function component_add(component_id, component_type)
{
	if ((component_type !== "group") && component_type !== "project")
		return Promise.reject("Illegal component type.");

	fetch(CI_API_V4_URL + "/" + component_type + "s/" + component_id + "/variables", {
		method: "GET",
		headers: gitlab_http_headers,
	})
	.then(response => {
		if (check_fetch_response(component_id, component_type + "s/variables-api", response)) {
			response.json().then(component_json => {
				if (component_json.find(opt => (opt.key === "DASHBOARD_OPT_OUT") &&
				                               (opt.value === "true")))
					return;

				/*
				 * Ensure the loop can always run, because of
				 * default_opt_out = false and an empty array.
				 */
				if (!component_json.length)
					component_json = [{ key: "", value: "" }];

				for (const variable of component_json) {
					if ((!dashboard_default_opt_out ||
					    ((variable.key === "DASHBOARD_OPT_IN") &&
					     (variable.value === "true"))) &&
					     !dashboard_dynamic_projects.some(component => (component.id === component_id) &&
					                                                   (!component.branch)))
						dashboard_dynamic_projects.push({ id: component_id });

					if ((component_type === "project") &&
					    (variable.key === "DASHBOARD_TILE_BRANCHES")) {
						for (const branch_name of JSON.parse(variable.value)) {
							if ((typeof(branch_name) === "string") &&
							    !dashboard_dynamic_projects.some(project =>
								    (project.id === component_id) && (project.branch === branch_name)))
								dashboard_dynamic_projects.push({ id: component_id, branch: branch_name });
						}
					}
				}
			}
		)}
	}).catch(error => handle_fetch_error(error));
}

/**
 * components_add() - Add a list of components
 * components         An array of Gitlab group or project objects
 * component_type     Type of component being added ("group", "project")
 *
 * From an array of component objects, add each individual one.
 *
 * Note, that the array of objects are expected to be all of the same type,
 * as normally returned by the GitLab API.
 */
function components_add(components, component_type)
{
	if (Array.isArray(components)) {
		for (const component of components) {
			if (Number.isInteger(component.id))
				component_add(component.id, component_type);
		}
	}

	return Promise.resolve(null);
}

/**
 * check_fetch_group() - Fetch group data if @group_id is a group
 * group_id              The ID to check whether it is a group
 *
 * Checks the ID whether it is a group ID or not. If it is a group ID, it
 * gets the group data from the GitLab API and adds any projects to the list
 * of dynamic projects. Because an ID can also be a sub-group, which we also
 * want to add to our list of ID's, we have to fetch subgroups for each
 * provided group ID.
 */
function check_fetch_group(group_id)
{
	fetch(CI_API_V4_URL + "/groups/" + group_id + "?all_available=true", {
		method: "GET",
		headers: gitlab_http_headers,
	})
	.then(response => {
		if (response.status === 404)
			console.log("debug: ID '" + group_id + "' is neither a project nor group ID.");

		if (check_fetch_response(group_id, "group-api", response))
			response.json().then(group_data => components_add(group_data.projects, "project"));

		fetch(CI_API_V4_URL + "/groups/" + group_id + "/subgroups?all_available=true", {
			method: "GET",
			headers: gitlab_http_headers,
		})
		.then(response => {
			if (check_fetch_response(group_id, "subgroup-api", response))
				response.json().then(subgroup_data => components_add(subgroup_data, "group"));
		});
	}).catch(error => handle_fetch_error(error));

	return Promise.resolve(null);
}

/**
 * fetch_project_data() - Fetch project data from the GitLab API
 * project_id:            The ID of the project to fetch
 *
 * Get the project data block from the API.
 *
 * Returns the promise of the json object containing project data.
 */
function fetch_project_data(project_id) {
	return fetch(CI_API_V4_URL + "/projects/" + project_id, {
		method: "GET",
		headers: gitlab_http_headers,
	})
	.then(response => {
		if (response.status === 404)
			return check_fetch_group(project_id);
		else
			return check_fetch_response(project_id, "project-api", response).json();
	});
}

/**
 * fetch_pipelines_data() - Fetch pipelines from the GitLab API
 * project_data:           Previously retrieved project data in
 * dashboard_branch:       Optional branch name to get the pipeline status for
 *
 * Get the project data block from the API.
 *
 * Returns the promise of the json object containing project data.
 */
function fetch_pipelines_data(project_data, dashboard_branch) {
	const branch = dashboard_branch != null ? dashboard_branch : project_data.default_branch;

	return fetch(CI_API_V4_URL + "/projects/" + project_data.id + "/pipelines/?per_page=1&ref=" + branch, {
		method: "GET",
		headers: gitlab_http_headers,
	})
	.then(response => check_fetch_response(project_data.id, "pipelines-api", response).json());
}

/**
 * fetch_pipeline_data() - Fetch pipeline from the GitLab API
 * project_data:           Previously retrieved project data
 * pipelines+data:         Previously retrieved pipelines data
 */
function fetch_pipeline_data(project_data, pipelines_data) {
	if (!Array.isArray(pipelines_data) ||
	    (pipelines_data.length < 1))
		return Promise.resolve(null);

	return fetch(CI_API_V4_URL + "/projects/" + project_data.id + "/pipelines/" + pipelines_data[0].id, {
		method: "GET",
		headers: gitlab_http_headers
	})
	.then(response => check_fetch_response(project_data.id, "pipeline-api", response).json());
}

/* Static variable to loop over all projects wanting to be rendered */
let dashboard_index = 0;
/* Static variable to contain a lock on the fetch function to avoid races */
let pipeline_fetch_lock = false;
/**
 * fetch_and_render_data() - Retrieve all pipeline related data from GitLab
 *
 * Fetches all data using the GitLab API and renders the dashboard based on the
 * result. The algorithm is throttled in so that it renders only one tile per
 * call from the available dashboard_projects.
 *
 * The error handling is setup in such a way, that if there are errors getting
 * data from the API, the page is eventually reloaded, forcing a
 * 're-initialization'.
 */
function fetch_and_render_data()
{
	const timestamp = new Date();
	const dashboard_projects = [].concat(dashboard_static_projects, dashboard_dynamic_projects);
	let dashboard_project = dashboard_projects[dashboard_index];
	let project_data = null;

	if (pipeline_fetch_lock)
		return;

	pipeline_fetch_lock = true;

	dashboard_project.id = parseInt(dashboard_project.id);

	fetch_project_data(dashboard_project.id)
		.then(__project_data => {
			project_data = __project_data;

			if (project_data == null)
				return Promise.resolve(null);

			return fetch_pipelines_data(project_data, dashboard_project.branch);
		})
		.then(pipelines_data => {
			if (pipelines_data == null)
				return Promise.resolve(null);

			return fetch_pipeline_data(project_data, pipelines_data);
		})
		.then(pipeline_data => {
			if (pipeline_data == null)
				return Promise.resolve(null);

			dashboard_index -= remove_duplicates(dashboard_dynamic_projects);
			if (dashboard_index < 0)
				dashboard_index = 0;

			return render_dashboard_content(project_data, pipeline_data, timestamp);
		})
		.catch(error => handle_fetch_error(error));

	dashboard_index = (++dashboard_index) % dashboard_projects.length;
	pipeline_fetch_lock = false;
}

/**
 * render_dashboard_header() - Render the static dashboard header
 * dashboard_data:             GitLab project object containing the dashboard data
 *
 * Sets up the static content of the dashboard, such as the page title and the
 * logo and name of this dashboard, as stored in a GitLab project object.
 */
function render_dashboard_header(dashboard_data)
{
	let heading = null;
	let heading_link = null;
	let logo = null;
	let logo_link = null;

	document.getElementById("page-title").textContent = dashboard_data.name;

	logo = document.createElement("img");
	logo.alt = dashboard_data.name_with_namespace;
	logo.className = "banner-img";
	logo.src = ((typeof DASHBOARD_BANNER_LOGO !== "undefined") &&
	            DASHBOARD_BANNER_LOGO &&
	            (DASHBOARD_BANNER_LOGO.length > 0)) ?
	           DASHBOARD_BANNER_LOGO :
	           dashboard_data.avatar_url;

	logo_link = document.createElement("a");
	logo_link.href = dashboard_data.web_url;
	logo_link.appendChild(logo);

	logo.addEventListener("error", () => logo_link.remove());
	document.getElementById("logo").appendChild(logo_link);

	heading = document.createElement("h1");
	heading.textContent = dashboard_data.name;

	heading_link = document.createElement("a");
	heading_link.href = dashboard_data.web_url;
	heading_link.appendChild(heading);

	document.getElementById("heading").appendChild(heading_link);
}

/**
 * fetch_project_metadata() - Retrieve all project related data from GitLab
 *
 * Fetching the dashboard project data from a GitLab repository, usually the
 * repository hosting this (or a fork/sub-module) of this code as a 'pages'
 * pipeline. The logo, link and name from that project are used for the page
 * title/header.
 */
function fetch_project_metadata()
{
	if ((typeof DASHBOARD_FULL_RELOAD_TIMEOUT === "number") &&
	    (DASHBOARD_FULL_RELOAD_TIMEOUT > 10000))
		setInterval(window.location.reload, DASHBOARD_FULL_RELOAD_TIMEOUT);

	update_heartbeat_tile(new Date());
	document.getElementById("version").textContent = "Dashboard config version: " +
	                                                 (((typeof DASHBOARD_CONFIG_VERSION !== "undefined") &&
	                                                   DASHBOARD_CONFIG_VERSION &&
	                                                  (DASHBOARD_CONFIG_VERSION.length > 0)) ?
	                                                  DASHBOARD_CONFIG_VERSION : "unknown");
	document.getElementById("identifier").textContent = ((typeof DASHBOARD_CONFIG_IDENTIFIER !== "undefined") &&
	                                                     DASHBOARD_CONFIG_IDENTIFIER &&
	                                                     (DASHBOARD_CONFIG_IDENTIFIER.length > 0)) ?
	                                                    DASHBOARD_CONFIG_IDENTIFIER :
	                                                    "unknown identifier";
	if (typeof CI_API_V4_URL === "undefined") {
		handle_fetch_error("CI_API_V4_URL undefined");
		return;
	}

	if (typeof DASHBOARD_ID  === "undefined") {
		handle_fetch_error("DASHBOARD_ID undefined");
		return;
	}

	fetch(CI_API_V4_URL + "/projects/" + DASHBOARD_ID, {
		method: "GET",
		headers: gitlab_http_headers
	}).then(response => check_fetch_response("banner", "banner-api", response)).then(response => response.json()).then(dashboard_data =>
		render_dashboard_header(dashboard_data)
	).catch(response => handle_fetch_error(response));
}

/**
 * load_override_stylesheet() - Add additional stylesheets
 * stylesheet                   The stylesheet content to add to the document
 *
 * Add a new stylesheet node to the document, based on the supplied content.
 * This is useful when a particular style-snippet is needed to override
 * something.
 */
function load_override_stylesheet(stylesheet)
{
	let style = document.createElement("style");

	style.innerText = stylesheet;
	style.type = "text/css";

	document.head.appendChild(style);
}

/**
 * enable_custom_theme() - Enable an optional custom dashboard theme
 * custom_theme:           String containing custom theme name or `.css` href
 *
 * A custom dashboard theme could have been configured by the user, if it is
 * a valid theme, we should attempt to enable it.
 *
 */
function enable_custom_theme(custom_theme)
{
	let style_title = null;

	if (custom_theme.endsWith(".css")) {
		document.getElementById("custom_style").href = custom_theme + ".css";
		style_title = "Custom style";
	} else  {
		style_title = custom_theme;
	}

	for (let stylesheet of document.getElementsByTagName("link")) {
		if ((stylesheet.getAttribute("rel").indexOf("style") != -1) &&
		    (stylesheet.getAttribute("title") == style_title))
			stylesheet.disabled = false;
		else
			stylesheet.disabled = true;
	}
}

/**
 * pipeshow() - Main entry function
 *
 * Create and run the dashboard, first the project data, such as the name and
 * logo, followed by a first rendering of the first tile, and then let the
 * automatic interval take over.
 */
function pipeshow()
{
	if ((typeof DASHBOARD_THEME !== "undefined") &&
	    (DASHBOARD_THEME) &&
	    (DASHBOARD_THEME.length > 0))
		enable_custom_theme(DASHBOARD_THEME);

	if ((typeof DASHBOARD_STYLE_OVERRIDE  !== "undefined") &&
	    (DASHBOARD_STYLE_OVERRIDE) &&
	    (DASHBOARD_STYLE_OVERRIDE.length > 0))
		load_override_stylesheet(DASHBOARD_STYLE_OVERRIDE);

	fetch_project_metadata();

	if ((!dashboard_static_projects ||
	    (dashboard_static_projects.length <= 0)) &&
	    (!dashboard_dynamic_projects ||
	    (dashboard_dynamic_projects.length <= 0))
	) {
		document.getElementById("time").textContent = "No projects defined!";
		document.getElementById("date").innerHTML = "Check <em>config.js</em>";

		return;
	}

	fetch_and_render_data();
	setInterval(fetch_and_render_data, ((typeof DASHBOARD_INTERVAL_TIMEOUT === "number") &&
	                                    (DASHBOARD_INTERVAL_TIMEOUT > 500)) ?
	                                   DASHBOARD_INTERVAL_TIMEOUT :
	                                   5000);
}
/* exported pipeshow */
