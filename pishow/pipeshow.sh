#!/bin/sh

set -eu

INDEX="$(mktemp)"

cleanup()
{
	if [ -f "${INDEX}" ]; then
		unlink "${INDEX}"
	fi
}

trap cleanup EXIT

aplay resources/imperial_march.wav &

wget https://ultimaker.gitlab.io/index.html -O "${INDEX}"

unclutter -root -idle 5 &
chromium-browser --incognito --kiosk --noerrdialogs --disable-translate --disable-cache --disk-cache-dir=/dev/null --disk-cache-size=1 --app="file://${INDEX}"

cleanup

exit 0
